import os

# numpy
import numpy as np

# astropy
import astropy.units as u
import astropy.wcs as fitswcs
from astropy.modeling import models
from astropy import constants as const

# specutils
from specutils import Spectrum1D

# matplotlib
import matplotlib.pyplot as plt

from scipy.optimize import fsolve

"""
SB2
Permet de générer des spectres synthétique d'étoile binaire SB2 
en fonction de paramètres orbitaux données.
Génère les spectres individuels de l'étoile A et B puis les combines afin 
d'obtenir un spectre composite. 
"""


class SB2:
    def __init__(self, q, K, e, omega, P, T0, gamma):
        self.K = K
        self.e = e
        self.omega = omega
        self.P = P
        self.T0 = T0
        self.gamma = gamma
        self.q = q
        # calculate the argument of periastron for the secondary star
        self.omega_2 = self.omega + 180

    def _true_anom(self, t):
        t = (t - self.T0) % self.P
        def f(E): return E - self.e * np.sin(E) - 2 * np.pi * t/self.P
        E0 = 2 * np.pi * t / self.P
        E = fsolve(f, E0)[0]
        th = 2 * np.arctan(np.sqrt((1 + self.e)/(1 - self.e)) * np.tan(E/2.))
        if E < np.pi:
            return th
        else:
            return th + 2 * np.pi

    def _v1_f(self, f):
        return self.K * (np.cos(self.omega * np.pi/180 + f) + self.e * np.cos(self.omega * np.pi/180))

    def _vA_t(self, t):
        f = self._true_anom(t)
        return self._v1_f(f) + self.gamma

    def _v2_f(self, f):
        return self.K/self.q * (np.cos(self.omega_2 * np.pi/180 + f) + self.e * np.cos(self.omega_2 * np.pi/180))

    def _vB_t(self, t):
        f = self._true_anom(t)
        return self._v2_f(f) + self.gamma

    def _get_velocities(self, dates):
        dates = np.atleast_1d(dates)
        vAs = np.array([self._vA_t(date) for date in dates])
        vBs = np.array([self._vB_t(date) for date in dates])
        return np.vstack((vAs, vBs))

    def generate(self, output_dir="synthetic_sb2", contrast_A=.68, contrast_B=.38, fwhm_A_Lorentz=6, fwhm_A_Gaussian=.5, fwhm_B=6, snr=300, n_samples=50, rest=6562.82, region=(6500, 6600), n=0.1):
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        c = const.c.to('km/s')
        x = np.arange(region[0], region[1], n)
        dates = np.linspace(0, self.P, n_samples)
        vrA, vrB = self._get_velocities(dates)
        cwcs = fitswcs.WCS(
            header={'CDELT1': n, 'CRVAL1': x[0], 'CUNIT1': 'Angstrom', 'CRPIX1': 1.})

        deltaWvA, deltaWvB = None, None
        with open(os.path.join(output_dir, 'synth_data.txt'), 'w') as f:
            fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)
            for i, vr in enumerate(vrA):
                A_wv = rest + (vrA[i] / c * rest).value
                B_wv = rest + (vrB[i] / c * rest).value
                if i == 0:
                    deltaWvA, deltaWvB = (
                        (vrA[i]) / c * rest).value, ((vrB[i]) / c * rest).value
                line_A = models.Voigt1D(
                    A_wv, -contrast_A, fwhm_A_Lorentz, fwhm_A_Gaussian)

                y_A = line_A(x) + \
                    np.random.normal(1., 1/snr, x.shape)

                A = Spectrum1D(flux=y_A*u.Jy, wcs=cwcs)

                line_B = models.Gaussian1D(-contrast_B,  B_wv,
                                           fwhm_B / 2 * np.sqrt(2 * np.log(2)))

                y_B = line_B(x) + \
                    np.random.normal(1., 1/snr, x.shape)
                B = Spectrum1D(flux=y_B*u.Jy, wcs=cwcs)
                if i == 0:
                    output_fn = os.path.join(
                        output_dir, f'__individual_A_{i}.fits')
                    A.write(output_fn, overwrite=True, format="wcs1d-fits")
                    output_fn = os.path.join(
                        output_dir, f'__individual_B_{i}.fits')
                    B.write(output_fn, overwrite=True, format="wcs1d-fits")

                wcs = fitswcs.WCS(
                    header={'CDELT1': n, 'CRVAL1': x[0], 'CUNIT1': 'Angstrom', 'CRPIX1': 1.})
                AB = Spectrum1D(flux=A.flux + B.flux - 2 * u.Jy, wcs=wcs)
                output_fn = os.path.join(
                    output_dir, f'synth_{i}.fits')
                AB.write(output_fn, overwrite=True, format="wcs1d-fits")

                output = f"{os.path.basename(output_fn)} {vrA[i]}"
                f.write(output + '\n')
                if i % 2:
                    ax1.plot(x, A.flux + i*0.05*u.Jy, 'r-')
                    ax1.plot(x, B.flux + i*0.05*u.Jy, 'b--')
                    ax2.plot(x, (A.flux + B.flux)/2 + i*.05*u.Jy, 'k-')
            plt.tight_layout(pad=1, w_pad=0, h_pad=0)
            plt.grid(True)
            plt.show()
        return (deltaWvA, deltaWvB)
