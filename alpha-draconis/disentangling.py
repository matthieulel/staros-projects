import os
import glob


# numpy
import numpy as np

# astropy
from astropy.io import fits
import astropy.units as u
from astropy.coordinates import SpectralCoord
from astropy import constants as const
import astropy.wcs as fitswcs

# specutils
from specutils import Spectrum1D
from specutils.manipulation import LinearInterpolatedResampler


# matplotlib
import matplotlib.pyplot as plt

from sb_factory import SB2


def spectralDisentangling(spec_path, pattern, rvfile, itercount=10, q=None, v_gamma=0, rest=6562.82, sample=.05, region=(6520, 6590), verbose=True):
    c = const.c.to('km/s')
    spectra, ss = {}, {}
    region = np.arange(region[0], region[1], sample)
    sc = SpectralCoord(region, unit='AA')
    linear = LinearInterpolatedResampler(extrapolation_treatment='zero_fill')

    # ------------------------------

    def loadSpectra():
        for f in sorted(glob.glob(os.path.join(spec_path, pattern))):
            spectrum_file = fits.open(f)
            header = spectrum_file[0].header
            wcs_data = fitswcs.WCS(header=header)
            flux = spectrum_file[0].data * u.Jy  # - 1 * u.Jy
            spectra[os.path.basename(f)] = {'spectrum': Spectrum1D(
                flux=flux, wcs=wcs_data), 'vra': None, 'vrb': None}

        with open(rvfile, "r") as file:
            for line in file.readlines():
                d = line.split(' ')
                rvb = float(d[2])*-1 if len(d) == 3 else None
                spectra[d[0]]['vra'] = float(d[1]) * -1
                spectra[d[0]]['vrb'] = rvb

    # ------------------------------

    def shiftAndAdd(composite_spec_A, composite_spec_B, what):
        ref_spec = None
        composite_spec = None
        for name, data in spectra.items():
            vra_wv = ((data['vra'] / c * rest).value) * u.AA
            if what == 'first':
                obs_shifted = Spectrum1D(
                    spectral_axis=data['spectrum'].spectral_axis + vra_wv,
                    flux=data['spectrum'].flux)
                obs_shifted = linear(
                    obs_shifted, sc)
                composite_spec = obs_shifted if not composite_spec else obs_shifted + \
                    composite_spec
            else:
                vrb = data['vrb'] if data['vrb'] != None else (
                    v_gamma - (data['vra'] - v_gamma) / q)

                vrb_wv = ((vrb / c * rest).value) * u.AA
                if not ref_spec:
                    ref_spec = composite_spec_B if what == 'A' else composite_spec_A
                s_shifted = Spectrum1D(
                    spectral_axis=ref_spec.spectral_axis -
                    vrb_wv + vra_wv,
                    flux=ref_spec.flux) if what == 'A' else Spectrum1D(
                    spectral_axis=ref_spec.spectral_axis -
                    vra_wv + vrb_wv,
                    flux=ref_spec.flux)
                s_shifted = linear(s_shifted, sc)
                vr = vra_wv if what == 'A' else vrb_wv
                obs_shifted = Spectrum1D(
                    spectral_axis=data['spectrum'].spectral_axis + vr,
                    flux=data['spectrum'].flux)
                obs_shifted = linear(obs_shifted, sc)
                ns = obs_shifted - s_shifted
                composite_spec = ns if not composite_spec else ns + composite_spec
        composite_spec = composite_spec / len(spectra)
        return composite_spec

    # ------------------------------

    loadSpectra()

    # -- STEP 1 --
    # reinforce the feature of star A
    # each spectra is corrected for the radial velocity
    # of star A and then co-added.
    A = shiftAndAdd(None, None, 'first')
    emissionA = None
    # -- STEP 2 --
    # first approx of A spectrum is substracted from each spectra
    # wich are then corrected for the radial velocity of B and
    # co-added to provide the first approx of star B
    B = shiftAndAdd(A, None, 'B')
    emissionB = B.flux.copy()
    emissionB[emissionB < 0] = 0

    # -- STEP 3 --
    # repeat the process to improve A and B spectrum approximation
    # save all results
    if not os.path.exists(os.path.join(spec_path, 'disentangling')):
        os.makedirs(os.path.join(spec_path, 'disentangling'))
    for ii in range(itercount):
        if verbose:
            print(ii)
        B = B + emissionA.value * u.Jy - emissionB.value * \
            u.Jy if emissionA else B - emissionB.value * u.Jy
        A = shiftAndAdd(None, B, 'A')
        emissionA = A.flux.copy()
        emissionA[emissionA < 0] = 0 * u.Jy

        # fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)
        # ax1.plot(B.spectral_axis, emissionB.value, 'b--')
        # ax1.plot(A.spectral_axis, emissionA.value, 'r-')
        # ax1.plot(A.spectral_axis, A.flux, 'k-')
        # ax1.plot(A.spectral_axis, A.flux + emissionB.value * u.Jy -
        #  emissionA.value * u.Jy, 'g--')
        A = A + emissionB.value * u.Jy  # - emissionA.value * u.Jy
        B = shiftAndAdd(A, None, 'B')
        emissionB = B.flux.copy()
        emissionB[emissionB < 0] = 0 * u.Jy

        writeSpectra(A, sample, region[0], os.path.join(spec_path,
                                                        f'disentangling/_A{ii+1}.fits'))
        writeSpectra(B, sample, region[0], os.path.join(spec_path,
                                                        f'disentangling/_B{ii+1}.fits'))

        # ax1.plot(A.spectral_axis, A.flux, 'k-')
        # ax2.plot(B.spectral_axis, emissionB.value, 'r-')
        # ax2.plot(B.spectral_axis, B.flux, 'k-')
        # # ax2.plot(A.spectral_axis, B.flux + emissionA.value *
        # #  u.Jy - emissionB.value * u.Jy, 'g--')
        # plt.show()

# ------------------------------


def writeSpectra(spectrum1D, cdelt1, crval1, filename):
    wcs = fitswcs.WCS(
        header={'cdelt1': cdelt1, 'crval1': crval1, 'CUNIT1': 'Angstrom', 'CRPIX1': 1.})
    spec = Spectrum1D(flux=spectrum1D.flux, wcs=wcs)
    spec.write(filename, overwrite=True, format="wcs1d-fits")


def loadSpectrum(filename):
    spectrum_file = fits.open(filename)
    header = spectrum_file[0].header
    wcs_data = fitswcs.WCS(header=header)
    flux = spectrum_file[0].data * u.Jy
    spectrum = Spectrum1D(flux=flux, wcs=wcs_data)
    return spectrum

# ------------------------------


def findMassRatio(spec_path, pattern, rvfile, itercount=10, q_range=None, v_gamma=0, rest=6562.82, sample=.05, region=(6520, 6590)):
    flux = 0
    best_q = None
    for q in q_range:
        spectralDisentangling(spec_path, pattern, rvfile,
                              itercount, q, v_gamma, rest, sample, region, verbose=False)
        r = loadSpectrum(os.path.join(
            spec_path, 'disentangling', f'_B{itercount}.fits'))
        f = r.flux.sum()
        if f < flux:
            flux = f
            best_q = q
        print(f'q={q} flux={f}')
    return best_q


def plotABResult(spectra_path, region, iternumber=[], q=None, deltaWvA=None, deltaWvB=None):
    c = const.c.to('km/s')
    fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)

    if deltaWvB and deltaWvA:
        # First synthetic spectra star A
        compositeA = loadSpectrum(os.path.join(
            spectra_path, f'__individual_A_0.fits'))
        compositeA = Spectrum1D(
            spectral_axis=compositeA.spectral_axis - deltaWvA * u.AA, flux=compositeA.flux)
        ax1.plot(compositeA.spectral_axis, compositeA.flux -
                 1 * u.Jy, 'k', lw=2, label="Star A0")
        # First synthetic spectra star B
        compositeB = loadSpectrum(os.path.join(
            spectra_path, f'__individual_B_0.fits'))
        compositeB = Spectrum1D(
            spectral_axis=compositeB.spectral_axis - deltaWvB * u.AA, flux=compositeB.flux)
        ax2.plot(compositeB.spectral_axis, compositeB.flux -
                 1 * u.Jy, 'k', lw=2, label="Star B0")

    # A
    for i, iternbr in enumerate(iternumber):
        print(i)
        r = loadSpectrum(os.path.join(
            spectra_path, 'disentangling', f'_A{iternbr}.fits'))
        if i + 1 < len(iternumber):
            ax1.plot(r.spectral_axis, r.flux, linestyle=(0, (5, 5)),
                     lw=1, label=f'Star A{iternbr}')
        else:
            linestyle = (0, (5, 5)) if deltaWvB else 'solid'
            color = 'red' if deltaWvB else 'black'
            ax1.plot(r.spectral_axis, r.flux, color=color, linestyle=linestyle,
                     lw=2, label=f'Star A{iternbr}')
    # B
    for i, iternbr in enumerate(iternumber):
        print(i)
        r = loadSpectrum(os.path.join(
            spectra_path, 'disentangling', f'_B{iternbr}.fits'))
        if i + 1 < len(iternumber):
            ax2.plot(r.spectral_axis, r.flux, linestyle=(0, (5, 5)),
                     lw=1, label=f'Star B{iternbr}')
        else:
            linestyle = (0, (5, 5)) if deltaWvB else 'solid'
            color = 'red' if deltaWvB else 'black'
            ax2.plot(r.spectral_axis, r.flux, color=color, linestyle=linestyle,
                     lw=2, label=f'Star B{iternbr}')
    if q:
        print(f'q={q} value={r.flux.sum()}')
    ax1.set_xlim(region)
    ax2.set_xlim(region)
    ax1.set_title('A disentangled spectrum')
    ax2.set_title('B disentangled spectrum')

    ax1.legend()
    ax2.legend()
    ax1.grid(True)
    ax2.grid(True)
    ax1.set_xlabel('Wavelength in Å', fontdict=None, labelpad=None)
    ax1.set_ylabel('Relative intensity', fontdict=None, labelpad=None)
    ax2.set_xlabel('Wavelength in Å', fontdict=None, labelpad=None)

    plt.tight_layout(pad=1, w_pad=0, h_pad=0)
    if (q):
        plt.close(fig)
    else:
        plt.show()

# ------------------------------


if (__name__ == "__main__"):
    synthetic_spectra_path = 'synthetic_sb2'
    rv_file = os.path.join(synthetic_spectra_path, 'synth_data.txt')
    pattern = 'synth_*.fit*'
    region = (6500, 6620)

    sb2 = SB2(q=1, gamma=0, K=200, e=0, omega=21.5, P=1, T0=0)
    sb2.generate(output_dir=synthetic_spectra_path,
                n_samples=150, 
                fwhm_A_Lorentz=6, 
                fwhm_A_Gaussian=.5, 
                fwhm_B=11, 
                contrast_B=0.1,
                contrast_A=0.68)

    # Run spectral disentangling
    spectralDisentangling(synthetic_spectra_path,
                          pattern, rv_file, itercount=50, region=region, q=q, v_gamma=-0)
    plotABResult(synthetic_spectra_path,
                 (6520, 6600), [1, 10, 30, 50], q=None, deltaWvA=deltaWvA, deltaWvB=deltaWvB)
