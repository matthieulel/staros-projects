"""
Created on Mon May 15 2023
@author: Guillaume Bertrand
"""

# matplotlib
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# numpy
import numpy as np

# astropy
from astropy.modeling import models
import astropy.units as u
from astropy import constants as const

# specutils
from specutils import Spectrum1D
from specutils.fitting import fit_lines

#


def computeConstA_Method1(rv, fwhm, snr, contrast, n):
    """
    Return a constant for a given radial velocity error in km.s-1 (J.W. BRAULT)
    """
    return rv / (fwhm / (contrast * snr * np.sqrt(n)))


def computeConstB_Method2(rv, fwhm, p, snr, contrast):
    """
    Return a constant for a given radial velocity error in km.s-1 (BOUCHY)
    """
    return rv / ((np.sqrt(fwhm) * np.sqrt(p)) / (contrast * snr))


def findCenterOfLine(spectrum1d, fwhm, model_type='gaussian'):
    """
    METHOD EXCTRACTED FROM spectroscopicbinarysystem MODULE
    Find the center of the line in a spectrum using a fit (models available : Gaussian1D, Lorentz1D)
    """
    ipeak = spectrum1d.flux.argmin()
    xpeak = spectrum1d.spectral_axis[ipeak].to(u.AA)

    s = spectrum1d - 1

    g_init = models.Gaussian1D(mean=xpeak, amplitude=s.flux.argmin(
    )) if model_type == 'gaussian' else models.Lorentz1D(amplitude=s.flux.argmin(), x_0=xpeak, fwhm=fwhm)
    g_fit = fit_lines(s, g_init, window=[
                      spectrum1d.spectral_axis.argmin(), spectrum1d.spectral_axis.argmax()])
    y_fit = g_fit(s.spectral_axis)

    center = g_fit.mean if model_type == 'gaussian' else g_fit.x_0

    return (center.value, y_fit + 1*u.Jy)

#


def runMonteCarlo(n_samples, spectral_sample_value, contrast, snr, center, fwhm, model):
    """
    Generate n_samples synthetic spectra with random noise and return standard deviation
    """
    wavelength_window = (6530, 6590)
    scale = 1 / snr

    line = models.Gaussian1D(-contrast, center,
                             fwhm / 2 * np.sqrt(2 * np.log(2))) if model == 'gaussian' else models.Lorentz1D(-contrast, x_0=center, fwhm=fwhm)

    #

    x = np.arange(wavelength_window[0],
                  wavelength_window[1], spectral_sample_value)

    errors = []
    synthetic_spectrum, y_fit = None, None

    for i in range(n_samples):
        y = line(x) + np.random.normal(1., scale, x.shape)
        spectrum = Spectrum1D(flux=y*u.Jy, spectral_axis=x*u.AA)
        r = findCenterOfLine(spectrum, fwhm, model)
        errors.append(r[0]-center)
        if i == 0:
            synthetic_spectrum = spectrum
            y_fit = r[1]

        std = np.std(errors)
        # print(f'{i+1}/{n_samples} std={std}')
    return std, synthetic_spectrum, y_fit


def getRV(std, lambda_ref):
    c = const.c.to('km/s')
    return (c * std / lambda_ref)


def run(model, n_samples, snr=150, contrast=.68, spectral_sample_value=.1, fwhm=1):
    center = 6560.123

    # run monteCarlo
    std, synthetic_spectrum, y_fit = runMonteCarlo(
        n_samples=n_samples,
        spectral_sample_value=spectral_sample_value,
        contrast=contrast,
        snr=snr,
        center=center,
        fwhm=fwhm,
        model=model)

    # compute A constant with first method
    a = computeConstA_Method1(
        rv=getRV(std, center),
        fwhm=getRV(fwhm, center),
        snr=snr,
        contrast=contrast,
        n=fwhm/spectral_sample_value)

    # compure B constant with second method
    b = computeConstB_Method2(
        rv=getRV(std, center),
        fwhm=getRV(fwhm, center),
        p=getRV(spectral_sample_value, center),
        snr=snr,
        contrast=contrast)

    return (a, b, std, synthetic_spectrum, y_fit, getRV(std, center))

# -----------------------------------------------------------------


def testWithMultipleFWHMandSNR(model='gaussian', n_samples=1000):
    plt.rcParams['font.size'] = '8'
    plt.rcParams['font.family'] = 'Georgia'
    fig, ((ax2, ax3, ax4)) = plt.subplots(3, 1, figsize=(4.5, 5), sharex=True)
    #
    a_arr, b_arr, std_arr, data = [], [], [], []
    fwhm_arr = np.arange(.5, 3.5, .5)
    snr_arr = np.linspace(50, 400, 8)
    c_arr = np.linspace(0.3,0.9,5)
    spectral_sample_value = np.linspace(0.1, 0.3, 5)

    cmap = plt.get_cmap('tab20c')
    colors = cmap((np.arange(8)).astype(int), alpha=1)

    for ii, f in enumerate(fwhm_arr):
        for i, s in enumerate(snr_arr):
            for z, r in enumerate(spectral_sample_value):
                for zz, c in enumerate(c_arr):
                    print(f'fwhm:{f} snr:{s} r:{r}')
                    a, b, std, spectrum, y_fit, rv = run(
                        model, n_samples=n_samples, contrast=c, spectral_sample_value=r, snr=s, fwhm=f)
                    a_arr.append(a)
                    b_arr.append(b)
                    std_arr.append(std)
                    data.append([a.value, std, rv.value, f, s, r, c])
                    ax2.plot(rv, r, color=colors[i], marker="o", markersize=3)
                    ax3.plot(rv, f, color=colors[i], marker="o", markersize=3)
                    if ii==0 and z==0 and zz==0:
                        ax4.plot(rv, c, color=colors[i], marker="o", markersize=3, label=f'SNR {s}')
                    else:
                        ax4.plot(rv, c, color=colors[i], marker="o", markersize=3)

    np.savetxt(f'output/2_rv_error_estimator_{model}_{n_samples}_{round(np.mean(a_arr),3)}_{round(np.std(a_arr),3)}.txt',np.array(data))
    lines_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
    lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]
    fig.legend(lines, labels, loc='outside upper center',ncol=4)
    ax2.set_ylabel('Spectral sampling (A)', size=8)
    ax3.set_ylabel('FWHM (A)', size=8)
    ax4.set_xlabel('RV error (km/s)', size=8)
    ax4.set_ylabel('Contrast', size=8)
    ax2.grid(True)
    ax3.grid(True)
    ax4.grid(True)
    ax2.margins(0.08) 
    ax3.margins(0.08) 
    ax4.margins(0.08) 
    plt.subplots_adjust(top=0.891,
                        bottom=0.101,
                        left=0.134,
                        right=0.975,
                        hspace=0.163,
                        wspace=0.2) 
    print(f'A, mean={round(np.mean(a_arr),3)}±{round(np.std(a_arr),3)}')
  
    plt.savefig(
        f'output/2_rv_error_estimator_{model}_{n_samples}_{round(np.mean(a_arr),3)}_{round(np.std(a_arr),3)}.png', dpi=150)
    plt.savefig(
        f'output/2_rv_error_estimator_{model}_{n_samples}_{round(np.mean(a_arr),3)}_{round(np.std(a_arr),3)}.eps')
    plt.show()

    
# -----------------------------------------------------------------------------------

if __name__ == '__main__':
    testWithMultipleFWHMandSNR('gaussian', n_samples=1000)
    # testWithMultipleFWHMandSNR('voigt', n_samples=1000)
    # testWithMultipleFWHMandSNR('lorentz', n_samples=1000)